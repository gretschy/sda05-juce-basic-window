//
//  ButtonRow.cpp
//  JuceBasicWindow
//
//  Created by Tom on 20/10/2016.
//
//

#include "ButtonRow.hpp"
#include "SimpleButton.hpp"


ButtonRow::ButtonRow()
{
    for (int i=0; i < 5; i++)
    {
        addAndMakeVisible(buttons[i]);
    }

}

ButtonRow::~ButtonRow()
{
    
}

void ButtonRow::resized()
{
    buttons[0].setBounds(0, 0, 70, 70);
    buttons[1].setBounds(((getWidth() - 70)/4), (getHeight() - 70)/4, 70, 70);
    buttons[2].setBounds((((getWidth() - 70)/4) * 2), ((getHeight() - 70)/4 * 2), 70, 70);
    buttons[3].setBounds((((getWidth() - 70)/4) * 3), ((getHeight() - 70)/4 * 3), 70, 70);
    buttons[4].setBounds((getWidth() - 70), getHeight() - 70, 70, 70);
    
    DBG(getWidth());
    DBG(getHeight());
}