//
//  ButtonRow.hpp
//  JuceBasicWindow
//
//  Created by Tom on 20/10/2016.
//
//

#ifndef ButtonRow_hpp
#define ButtonRow_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "SimpleButton.hpp"

class ButtonRow: public Component
{
public:
    ButtonRow();
    ~ButtonRow();
    
    void resized() override;
    
    
private:
    SimpleButton buttons[5];

};

#endif /* ButtonRow_hpp */
