/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    colour = Colours::seagreen;
    xCoord = yCoord = 0;

    addAndMakeVisible(myButtons);
}

MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{
    myButtons.setBounds(1, 1, getWidth(), getHeight());
}

void MainComponent::paint(Graphics& g)
{
    g.setColour(colour);
}


void MainComponent::mouseUp(const MouseEvent &event)
{
    xCoord = event.x;
    yCoord = event.y;
    
    DBG("Mouse released");
    repaint();
}

void MainComponent::buttonClicked(Button* button)
{
    
}


