/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "SimpleButton.hpp"
#include "ButtonRow.hpp"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                        public Button::Listener


{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();

    void resized() override;
    void paint(Graphics& g) override;
    void buttonClicked(Button* button) override;
    void mouseUp(const MouseEvent &event) override;
    

private:
    int xCoord;
    int yCoord;
    Colour colour;
    SimpleButton myButton;
    ButtonRow myButtons;
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
